---
title: About
layout: page
comments: false
---

This website is powered by [Hexo], based on [Hueman] Theme, hosted for free by [GitLab.com] with [GitLab Pages]!

Adaptation to GitLab by [@virtuacreative]

- View [GitLab CI] configuration
- [Fork me] on GitLab

Have fun! :)

[Virtua Creative]

[Hexo]: http://hexo.io/
[Hueman]: https://github.com/ppoffice/hexo-theme-hueman
[GitLab.com]: https://about.gitlab.com/gitlab-com/
[GitLab Pages]: http://doc.gitlab.com/ee/pages/README.html
[GitLab CI]: https://gitlab.com/themes-templates/hexo/blob/master/.gitlab-ci.yml
[Fork me]: https://gitlab.com/themes-templates/hexo/
[@virtuacreative]: https://gitlab.com/u/virtuacreative
[Virtua Creative]: https://virtuacreative.com.br/en/